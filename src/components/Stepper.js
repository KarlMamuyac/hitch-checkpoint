import React from 'react';
import { StepperTheme } from './Themes.js';
import { ThemeProvider } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import './Styles.css';

class Line1Stepper extends React.Component {
    state = {
        activeStep: this.props.currentActive,
        express1Marks: ['DLSU Laguna', 'Spine Road', 'Technopark Gate', 'Mamplasan', 'SLEX-Santo Tomas Service Area', 'SLEX-San Pedro Service Area',
            'SLEX-Susana Tollgate', 'SLEX-Filinvest Tollgate', 'SLEX-Sucat', 'Bicutan Station', 'Magallanes', 'Buendia', 'Gil Puyat', 'Vito Cruz', 'DLSU Manila'],
        dlsuLagunaPolygon: [[121.04229390621184,
            14.26254782316288], [121.04242265224455,
            14.262007119186643], [121.04398369789122,
            14.262355457473813], [121.04385495185852,
            14.262885762489335]],
        spineRoadPolygon: [[121.04963779449464,
            14.264237514728197], [121.04808211326599,
            14.263883980310183], [121.0481894016266,
            14.263478454853672], [121.04975581169127,
            14.26389437838921], [121.05126857757568,
            14.26440388367419], [121.05097889900208,
            14.264736621197713]],
        technoParkGate: [[121.04957342147827,
            14.268812615987503], [121.050066947937,
            14.267731236800719], [121.05350017547606,
            14.26902057292836], [121.05304956436156,
            14.269914785586709], [121.04957342147827,
            14.268812615987503]],
        mamplasanPolygon: [[121.07834815979002,
            14.306553653901137], [121.08135223388672,
            14.302894194177806], [121.08336925506592,
            14.30451600754497], [121.07972145080565,
            14.308217024981474], [121.07834815979002,
            14.306553653901137]],
        slexStoTomas: [[121.06987237930298,
            14.31004671894457], [121.07094526290892,
            14.308903161964137], [121.07392787933348,
            14.311294229010525], [121.07285499572754,
            14.312354607118484], [121.06987237930298,
            14.31004671894457]],
        slexSanPedro: [[121.04371547698976,
            14.356262335052696], [121.04382276535033,
            14.354724046820678], [121.04697704315184,
            14.355098226012089], [121.04695558547974,
            14.356636511673036], [121.04371547698976,
            14.356262335052696]],
        slexSusana: [[121.03817939758301,
            14.381351570332633], [121.03852272033691,
            14.379564029478733], [121.04311466217041,
            14.380561728462693], [121.04268550872803,
            14.38226612058891], [121.03817939758301,
            14.381351570332633]],
        slexFilinvest: [[121.04105472564696,
            14.412402612992693], [121.04058265686035,
            14.41119723144649], [121.04423046112059,
            14.409347581575117], [121.0448956489563,
            14.410906276303871], [121.04105472564696,
            14.412402612992693]],
        slexSucat: [[121.04313611984253,
            14.45494009126787], [121.04317903518675,
            14.45290379742439], [121.04744911193846,
            14.452945354628064], [121.04736328125,
            14.45491931285347], [121.04313611984253,
            14.45494009126787]],
        slexBicutan: [[121.04144096374512,
            14.487227398721375], [121.04210615158082,
            14.4851914009798], [121.04757785797118,
            14.486770339630993], [121.0469341278076,
            14.48884787335973], [121.04144096374512,
            14.487227398721375]],
        slexMagallanes: [[121.01410388946532,
            14.540094460350115], [121.01517677307129,
            14.537851242529623], [121.02049827575682,
            14.540385246177662], [121.01886749267578,
            14.54283614008546], [121.01410388946532,
            14.540094460350115]],
        buendia: [[121.00629329681396,
            14.557540932253723], [121.00682973861694,
            14.556772485737499], [121.00893259048462,
            14.557810926340256], [121.00833177566527,
            14.55872475002762], [121.00629329681396,
            14.557540932253723]],
        gilPuyat: [[120.99631547927856,
            14.555318660682982], [120.99650859832764,
            14.552826366877175], [120.99820375442505,
            14.553137905142133], [120.99781751632689,
            14.555526350562943], [120.99631547927856,
            14.555318660682982]],
        vitoCruz: [[121.00348234176636,
            14.567842010860629], [120.99356889724731,
            14.563418421128265], [120.99408388137819,
            14.562546153524115], [121.00393295288086,
            14.566803617485979], [121.00348234176636,
            14.567842010860629]],
        dlsuManila: [[120.99310755729674,
            14.566829577379949], [120.99245846271513,
            14.56653882639291], [120.99375128746031,
            14.563880513877775], [120.99440038204192,
            14.56415050020466], [120.99310755729674,
            14.566829577379949]],
        isInside: require('point-in-polygon'),
    };

    getIfInside(coords, polygon) {
        return (this.state.isInside(coords, polygon));
    };

    getStepContent() {
        switch (this.state.activeStep) {
            case 0:
                return ``;
            case 1:
                return '';
            case 2:
                return ``;
            case 3:
                return ``;
            case 4:
                return ``;
            case 5:
                return ``;
            case 6:
                return ``;
            case 7:
                return ``;
            case 8:
                return ``;
            case 9:
                return ``;
            case 10:
                return ``;
            case 11:
                return ``;
            case 12:
                return ``;
            default:
                return 'Unknown step';
        }
    };

    handleBack = () => {
        this.setState({ activeStep: this.state.activeStep - 1 });
    };

    handleReset = () => {
        this.setState({ activeStep: this.state.activeStep - this.state.activeStep });
    };

    handleNext = () => {
        this.setState({ activeStep: this.state.activeStep + 1 });
    };

    componentDidUpdate() {

        if (this.state.activeStep === 0) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.spineRoadPolygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 1) {
            if (this.props.lat === 14.262828572792786 && this.props.lng === 121.05417609214781) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 2) {
            if (this.props.lat === 14.253641639550501 && this.props.lng === 121.05948150157928) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 3) {
            if (this.props.lat === 14.250974395279833 && this.props.lng === 121.06347262859344) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 4) {
            if (this.props.lat === 14.251151172679108 && this.props.lng === 121.06486201286314) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 5) {
            if (this.props.lat === 14.264237514728197 && this.props.lng === 121.04963779449464) {
                this.handleReset();
            }
        }
    }

    render() {
        return (
            <ThemeProvider
                theme={StepperTheme()}>
                <div className="sliderRoot" >
                    <Stepper activeStep={this.state.activeStep} orientation="vertical">
                        {this.state.express1Marks.map((label, index) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                                <StepContent>
                                    <Typography>{this.getStepContent(index)}</Typography>
                                    <div className="actionsContainer">
                                        <div>
                                        </div>
                                    </div>
                                </StepContent>
                            </Step>
                        ))}
                    </Stepper>
                    {this.state.activeStep === this.state.express1Marks.length && (
                        <Paper square elevation={0} className="resetContainer">
                            <Typography>You&apos;ve arrived</Typography>
                        </Paper>
                    )}
                </div>
            </ThemeProvider>

        );
    };
}

class Line2Stepper extends React.Component {
    state = {
        activeStep: this.props.currentActive,
        express2Marks: ['Dlsu Laguna', 'Spine Road', 'Gate 2', '', 'Gate 1', 'Phoenix Paseo', 'Laguna Central'],
        // express4Marks: ['DLSU Laguna', 'Spine Road', 'Gate 2', 'Gate 1', 'Eton', 'Sta. Rosa Exit', 'Caltex'],
        // express5Marks: ['DLSU Laguna', 'Spine Road', 'Technopark Gate', 'Barangay Malamig', 'Service Road', 'Timbao', '7/11', 'Langkiwa'],
        dlsuLagunaPolygon: [[121.04253530502318,
            14.264767815315357], [121.04357600212097,
            14.260275817922176], [121.04436993598938,
            14.260473384504346], [121.04330778121947,
            14.264954979930538], [121.04253530502318,
            14.264767815315357]],
        spineRoadPolygon: [[121.04890823364256,
            14.26573483082058], [121.04992747306824,
            14.262948151232672], [121.0507321357727,
            14.263260094690258], [121.04974508285521,
            14.266015576480342], [121.04890823364256,
            14.26573483082058]],
        gate2Polygon: [[121.05289936065672,
            14.26260501293067], [121.05321049690245,
            14.261866743903818], [121.05568885803223,
            14.262823373728757], [121.05535626411437,
            14.26350964914547], [121.05289936065672,
            14.26260501293067]],
        halfwayMark: [[121.05519533157347,
            14.257967395553353], [121.0555064678192,
            14.257312298413607], [121.05772733688353,
            14.258102574091321], [121.0573625564575,
            14.258716075668147], [121.05519533157347,
            14.257967395553353]],
        gate1Polygon: [[121.05893969535826,
            14.254567108749344], [121.05738401412964,
            14.253610443893367], [121.05771660804749,
            14.253069718484149], [121.05931520462036,
            14.25407837829732], [121.05893969535826,
            14.254567108749344]],
        pheonixPolygon: [[121.063392162323,
            14.250917202562178], [121.0627645254135,
            14.250090506205586], [121.0632312297821,
            14.24976294643387], [121.0638588666916,
            14.250626039411104], [121.063392162323,
            14.250917202562178]],
        lagunaCentralPolygon: [[121.06434166431428,
            14.251135574678855], [121.0640197992325,
            14.250709228921195], [121.06494247913359,
            14.249996917747909], [121.06531798839569,
            14.250444062250851], [121.06434166431428,
            14.251135574678855]],
        isInside: require('point-in-polygon'),
    };

    getIfInside(coords, polygon) {
        return (this.state.isInside(coords, polygon));
    };

    getStepContent() {
        switch (this.state.activeStep) {
            case 0:
                return ``;
            case 1:
                return '';
            case 2:
                return ``;
            case 3:
                return ``;
            case 4:
                return ``;
            case 5:
                return ``;
            case 6:
                return ``;
            default:
                return 'Unknown step';
        }
    };

    handleBack = () => {
        this.setState({ activeStep: this.state.activeStep - 1 });
    };

    handleReset = () => {
        this.setState({ activeStep: this.state.activeStep - this.state.activeStep });
    };

    handleNext = () => {
        this.setState({ activeStep: this.state.activeStep + 1 });
    };

    componentDidUpdate() {

        if (this.state.activeStep === 0) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.spineRoadPolygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 1) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.gate2Polygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 2) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.halfwayMark)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 3) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.gate1Polygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 4) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.pheonixPolygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 5) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.lagunaCentralPolygon)) {
                this.handleNext();
            }
        }
    }

    render() {
        return (
            <ThemeProvider
                theme={StepperTheme()}>
                <div className="sliderRoot" >
                    <Stepper activeStep={this.state.activeStep} orientation="vertical">
                        {this.state.express2Marks.map((label, index) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                                <StepContent>
                                    <Typography>{this.getStepContent(index)}</Typography>
                                    <div className="actionsContainer">
                                        <div>
                                        </div>
                                    </div>
                                </StepContent>
                            </Step>
                        ))}
                    </Stepper>
                    {this.state.activeStep === this.state.express2Marks.length && (
                        <Paper square elevation={0} className="resetContainer">
                            <Typography>You&apos;ve arrived</Typography>
                        </Paper>
                    )}
                </div>
            </ThemeProvider>

        );
    };
}

class Line2StepperReturn extends React.Component {
    state = {
        activeStep: this.props.currentActive,
        express2Marks: ['Laguna Central', 'Phoenix Paseo', 'Gate 1', '', 'Gate 2', 'Spine Road', 'Dlsu Laguna'],
        // express3Marks: ['DLSU Laguna', 'Spine Road', 'Technopark Gate', 'Barangay Malamig', 'Verdana', 'Greenfield Rotonda', 'Mamplasan', 'Unilab', 'Brent', 'Pavillion Mall'],
        // express4Marks: ['DLSU Laguna', 'Spine Road', 'Gate 2', 'Gate 1', 'Eton', 'Sta. Rosa Exit', 'Caltex'],
        // express5Marks: ['DLSU Laguna', 'Spine Road', 'Technopark Gate', 'Barangay Malamig', 'Service Road', 'Timbao', '7/11', 'DLSU Langkiwa'],
        dlsuLagunaPolygon: [[121.04253530502318,
            14.264767815315357], [121.04357600212097,
            14.260275817922176], [121.04436993598938,
            14.260473384504346], [121.04330778121947,
            14.264954979930538], [121.04253530502318,
            14.264767815315357]],
        spineRoadPolygon: [[121.04890823364256,
            14.26573483082058], [121.04992747306824,
            14.262948151232672], [121.0507321357727,
            14.263260094690258], [121.04974508285521,
            14.266015576480342], [121.04890823364256,
            14.26573483082058]],
        gate2Polygon: [[121.05289936065672,
            14.26260501293067], [121.05321049690245,
            14.261866743903818], [121.05568885803223,
            14.262823373728757], [121.05535626411437,
            14.26350964914547], [121.05289936065672,
            14.26260501293067]],
        halfwayMark: [[121.05519533157347,
            14.257967395553353], [121.0555064678192,
            14.257312298413607], [121.05772733688353,
            14.258102574091321], [121.0573625564575,
            14.258716075668147], [121.05519533157347,
            14.257967395553353]],
        gate1Polygon: [[121.05893969535826,
            14.254567108749344], [121.05738401412964,
            14.253610443893367], [121.05771660804749,
            14.253069718484149], [121.05931520462036,
            14.25407837829732], [121.05893969535826,
            14.254567108749344]],
        pheonixPolygon: [[121.063392162323,
            14.250917202562178], [121.0627645254135,
            14.250090506205586], [121.0632312297821,
            14.24976294643387], [121.0638588666916,
            14.250626039411104], [121.063392162323,
            14.250917202562178]],
        lagunaCentralPolygon: [[121.06434166431428,
            14.251135574678855], [121.0640197992325,
            14.250709228921195], [121.06494247913359,
            14.249996917747909], [121.06531798839569,
            14.250444062250851], [121.06434166431428,
            14.251135574678855]],
        isInside: require('point-in-polygon'),
    };

    getIfInside(coords, polygon) {
        return (this.state.isInside(coords, polygon));
    };

    getStepContent() {
        switch (this.state.activeStep) {
            case 0:
                return ``;
            case 1:
                return '';
            case 2:
                return ``;
            case 3:
                return ``;
            case 4:
                return ``;
            case 5:
                return ``;
            case 6:
                return ``;
            default:
                return 'Unknown step';
        }
    };

    handleBack = () => {
        this.setState({ activeStep: this.state.activeStep - 1 });
    };

    handleReset = () => {
        this.setState({ activeStep: this.state.activeStep - this.state.activeStep });
    };

    handleNext = () => {
        this.setState({ activeStep: this.state.activeStep + 1 });
    };

    componentDidUpdate() {

        if (this.state.activeStep === 0) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.pheonixPolygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 1) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.gate1Polygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 2) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.halfwayMark)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 3) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.gate2Polygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 4) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.spineRoadPolygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 5) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.dlsuLagunaPolygon)) {
                this.handleNext();
            }
        }
    }

    render() {
        return (
            <ThemeProvider
                theme={StepperTheme()}>
                <div className="sliderRoot" >
                    <Stepper activeStep={this.state.activeStep} orientation="vertical">
                        {this.state.express2Marks.map((label, index) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                                <StepContent>
                                    <Typography>{this.getStepContent(index)}</Typography>
                                    <div className="actionsContainer">
                                        <div>
                                        </div>
                                    </div>
                                </StepContent>
                            </Step>
                        ))}
                    </Stepper>
                    {this.state.activeStep === this.state.express2Marks.length && (
                        <Paper square elevation={0} className="resetContainer">
                            <Typography>You&apos;ve arrived</Typography>
                        </Paper>
                    )}
                </div>
            </ThemeProvider>

        );
    };
}

class Line3Stepper extends React.Component {
    state = {
        activeStep: this.props.currentActive,
        express3Marks: ['DLSU Laguna', 'Spine Road', 'Technopark Gate', 'Gate 3', 'Verdana', 'Greenfield Rotonda', 'Mamplasan', 'Unilab Pharma Campus', 'Binan Welcome Arch', '', 'Pavillion Mall'],
        // express5Marks: ['DLSU Laguna', 'Spine Road', 'Gate 2', 'Gate 1', 'Eton', 'Sta. Rosa Exit', 'Caltex'],
        // express4Marks: ['DLSU Laguna', 'Spine Road', 'Technopark Gate', 'Barangay Malamig', 'Service Road', 'Timbao', '7/11', 'Langkiwa'],
        dlsuLagunaPolygon: [[121.04253530502318,
            14.264767815315357], [121.04357600212097,
            14.260275817922176], [121.04436993598938,
            14.260473384504346], [121.04330778121947,
            14.264954979930538], [121.04253530502318,
            14.264767815315357]],
        spineRoadPolygon: [[121.04890823364256,
            14.26573483082058], [121.04992747306824,
            14.262948151232672], [121.0507321357727,
            14.263260094690258], [121.04974508285521,
            14.266015576480342], [121.04890823364256,
            14.26573483082058]],
        technoParkGate: [[121.04957342147827,
            14.268812615987503], [121.050066947937,
            14.267731236800719], [121.05350017547606,
            14.26902057292836], [121.05304956436156,
            14.269914785586709], [121.04957342147827,
            14.268812615987503]],
        gate3Polygon: [[121.04957342147827,
            14.268812615987503], [121.050066947937,
            14.267731236800719], [121.05350017547606,
            14.26902057292836], [121.05304956436156,
            14.269914785586709], [121.04957342147827,
            14.268812615987503]],
        verdanaPolygon: [[121.06259822845458,
            14.290085615965399], [121.06530189514159,
            14.286675416980842], [121.06650352478026,
            14.28775670518239], [121.06319904327393,
            14.291000538604875], [121.06259822845458,
            14.290085615965399]],
        greenfieldRotonda: [[121.07229709625244,
            14.303102119622572], [121.07615947723389,
            14.298901988336], [121.07873439788818,
            14.30118919827459], [121.07409954071045,
            14.305056609399562], [121.07229709625244,
            14.303102119622572]],
        mamplasanPolygon: [[121.07834815979002,
            14.306553653901137], [121.08135223388672,
            14.302894194177806], [121.08336925506592,
            14.30451600754497], [121.07972145080565,
            14.308217024981474], [121.07834815979002,
            14.306553653901137]],
        unilabPolygon: [[121.08025789260866,
            14.31033780524638], [121.08360528945923,
            14.309506129097564], [121.08392715454102,
            14.31048334825583], [121.0804510116577,
            14.311252645453283], [121.08025789260866,
            14.31033780524638]],
        archPolygon: [[121.08225345611572,
            14.315026321915417], [121.08202815055849,
            14.314309016394999], [121.08394861221312,
            14.313706062155962], [121.08415246009825,
            14.314475348313954], [121.08225345611572,
            14.315026321915417]],
        halfway: [[121.08332633972168,
            14.32225122797938], [121.08753204345703,
            14.319839486632194], [121.08832597732544,
            14.321045360545297], [121.08439922332762,
            14.323353135658609], [121.08332633972168,
            14.32225122797938]],
        pavillionPolygon: [[121.086266040802,
            14.32842599932282], [121.0884439945221,
            14.326596455159995], [121.08896970748901,
            14.327230560224136], [121.08696341514587,
            14.328872987958098], [121.086266040802,
            14.32842599932282]],
        isInside: require('point-in-polygon'),
    };

    getIfInside(coords, polygon) {
        return (this.state.isInside(coords, polygon));
    };

    getStepContent() {
        switch (this.state.activeStep) {
            case 0:
                return ``;
            case 1:
                return '';
            case 2:
                return ``;
            case 3:
                return ``;
            case 4:
                return ``;
            case 5:
                return ``;
            case 6:
                return ``;
            case 7:
                return ``;
            case 8:
                return ``;
            case 9:
                return ``;
            case 10:
                return ``;
            default:
                return 'Unknown step';
        }
    };

    handleBack = () => {
        this.setState({ activeStep: this.state.activeStep - 1 });
    };

    handleReset = () => {
        this.setState({ activeStep: this.state.activeStep - this.state.activeStep });
    };

    handleNext = () => {
        this.setState({ activeStep: this.state.activeStep + 1 });
    };

    componentDidUpdate() {

        if (this.state.activeStep === 0) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.spineRoadPolygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 1) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.technoParkGate)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 2) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.gate3Polygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 3) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.verdanaPolygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 4) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.greenfieldRotonda)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 5) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.mamplasanPolygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 6) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.unilabPolygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 7) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.archPolygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 8) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.halfway)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 9) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.pavillionPolygon)) {
                this.handleNext();
            }
        }
    }

    render() {
        return (
            <ThemeProvider
                theme={StepperTheme()}>
                <div className="sliderRoot" >
                    <Stepper activeStep={this.state.activeStep} orientation="vertical">
                        {this.state.express3Marks.map((label, index) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                                <StepContent>
                                    <Typography>{this.getStepContent(index)}</Typography>
                                    <div className="actionsContainer">
                                        <div>
                                        </div>
                                    </div>
                                </StepContent>
                            </Step>
                        ))}
                    </Stepper>
                    {this.state.activeStep === this.state.express3Marks.length && (
                        <Paper square elevation={0} className="resetContainer">
                            <Typography>You&apos;ve arrived</Typography>
                        </Paper>
                    )}
                </div>
            </ThemeProvider>

        );
    };
}

class Line3StepperReturn extends React.Component {
    state = {
        activeStep: this.props.currentActive,
        express3Marks: ['Pavillion Mall', '', 'Binan Welcome Arch', 'Unilab Pharma Campus', 'Mamplasan', 'Greenfield Rotonda', 'Verdana', 'Gate 3', 'Technopark Gate', 'Spine Road', 'DLSU Laguna'],
        // express5Marks: ['DLSU Laguna', 'Spine Road', 'Gate 2', 'Gate 1', 'Eton', 'Sta. Rosa Exit', 'Caltex'],
        // express4Marks: ['DLSU Laguna', 'Spine Road', 'Technopark Gate', 'Barangay Malamig', 'Service Road', 'Timbao', '7/11', 'Langkiwa'],
        dlsuLagunaPolygon: [[121.04253530502318,
            14.264767815315357], [121.04357600212097,
            14.260275817922176], [121.04436993598938,
            14.260473384504346], [121.04330778121947,
            14.264954979930538], [121.04253530502318,
            14.264767815315357]],
        spineRoadPolygon: [[121.04890823364256,
            14.26573483082058], [121.04992747306824,
            14.262948151232672], [121.0507321357727,
            14.263260094690258], [121.04974508285521,
            14.266015576480342], [121.04890823364256,
            14.26573483082058]],
        technoParkGate: [[121.04957342147827,
            14.268812615987503], [121.050066947937,
            14.267731236800719], [121.05350017547606,
            14.26902057292836], [121.05304956436156,
            14.269914785586709], [121.04957342147827,
            14.268812615987503]],
        gate3Polygon: [[121.04957342147827,
            14.268812615987503], [121.050066947937,
            14.267731236800719], [121.05350017547606,
            14.26902057292836], [121.05304956436156,
            14.269914785586709], [121.04957342147827,
            14.268812615987503]],
        verdanaPolygon: [[121.06259822845458,
            14.290085615965399], [121.06530189514159,
            14.286675416980842], [121.06650352478026,
            14.28775670518239], [121.06319904327393,
            14.291000538604875], [121.06259822845458,
            14.290085615965399]],
        greenfieldRotonda: [[121.07229709625244,
            14.303102119622572], [121.07615947723389,
            14.298901988336], [121.07873439788818,
            14.30118919827459], [121.07409954071045,
            14.305056609399562], [121.07229709625244,
            14.303102119622572]],
        mamplasanPolygon: [[121.07834815979002,
            14.306553653901137], [121.08135223388672,
            14.302894194177806], [121.08336925506592,
            14.30451600754497], [121.07972145080565,
            14.308217024981474], [121.07834815979002,
            14.306553653901137]],
        unilabPolygon: [[121.08025789260866,
            14.31033780524638], [121.08360528945923,
            14.309506129097564], [121.08392715454102,
            14.31048334825583], [121.0804510116577,
            14.311252645453283], [121.08025789260866,
            14.31033780524638]],
        archPolygon: [[121.08225345611572,
            14.315026321915417], [121.08202815055849,
            14.314309016394999], [121.08394861221312,
            14.313706062155962], [121.08415246009825,
            14.314475348313954], [121.08225345611572,
            14.315026321915417]],
        halfway: [[121.08332633972168,
            14.32225122797938], [121.08753204345703,
            14.319839486632194], [121.08832597732544,
            14.321045360545297], [121.08439922332762,
            14.323353135658609], [121.08332633972168,
            14.32225122797938]],
        pavillionPolygon: [[121.086266040802,
            14.32842599932282], [121.0884439945221,
            14.326596455159995], [121.08896970748901,
            14.327230560224136], [121.08696341514587,
            14.328872987958098], [121.086266040802,
            14.32842599932282]],
        isInside: require('point-in-polygon'),
    };

    getIfInside(coords, polygon) {
        return (this.state.isInside(coords, polygon));
    };

    getStepContent() {
        switch (this.state.activeStep) {
            case 0:
                return ``;
            case 1:
                return '';
            case 2:
                return ``;
            case 3:
                return ``;
            case 4:
                return ``;
            case 5:
                return ``;
            case 6:
                return ``;
            case 7:
                return ``;
            case 8:
                return ``;
            case 9:
                return ``;
            case 10:
                return ``;
            default:
                return 'Unknown step';
        }
    };

    handleBack = () => {
        this.setState({ activeStep: this.state.activeStep - 1 });
    };

    handleReset = () => {
        this.setState({ activeStep: this.state.activeStep - this.state.activeStep });
    };

    handleNext = () => {
        this.setState({ activeStep: this.state.activeStep + 1 });
    };

    componentDidUpdate() {

        if (this.state.activeStep === 0) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.halfway)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 1) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.archPolygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 2) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.unilabPolygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 3) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.mamplasanPolygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 4) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.greenfieldRotonda)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 5) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.verdanaPolygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 6) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.gate3Polygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 7) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.technoParkGate)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 8) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.spineRoadPolygon)) {
                this.handleNext();
            }
        } else if (this.state.activeStep === 9) {
            if (this.getIfInside([this.props.lng, this.props.lat], this.state.dlsuLagunaPolygon)) {
                this.handleNext();
            }
        }
    }

    render() {
        return (
            <ThemeProvider
                theme={StepperTheme()}>
                <div className="sliderRoot" >
                    <Stepper activeStep={this.state.activeStep} orientation="vertical">
                        {this.state.express3Marks.map((label, index) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                                <StepContent>
                                    <Typography>{this.getStepContent(index)}</Typography>
                                    <div className="actionsContainer">
                                        <div>
                                        </div>
                                    </div>
                                </StepContent>
                            </Step>
                        ))}
                    </Stepper>
                    {this.state.activeStep === this.state.express3Marks.length && (
                        <Paper square elevation={0} className="resetContainer">
                            <Typography>You&apos;ve arrived</Typography>
                        </Paper>
                    )}
                </div>
            </ThemeProvider>

        );
    };
}

export { Line1Stepper, Line2Stepper, Line2StepperReturn, Line3Stepper, Line3StepperReturn };
