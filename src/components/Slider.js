import React from 'react';
import Slider from '@material-ui/core/Slider';


class DiscreteSlider extends React.Component {
    state = {
        express1marks: [
            {
                value: 0,
                label: 'DLSU Laguna',
            },
            {
                value: 25,
                label: 'Mamplasan',
            },
            {
                value: 50,
                label: 'SLEX',
            },
            {
                value: 75,
                label: 'Skyway',
            },
            {
                value: 100,
                label: 'DLSU Manila',
            },
        ],

        express2marks: [
            {
                value: 0,
                label: 'DLSU Laguna',
            },
            {
                value: 25,
                label: 'Technopark Gate',
            },
            {
                value: 50,
                label: 'Gate 2',
            },
            {
                value: 75,
                label: 'Gate 1',
            },
            {
                value: 100,
                label: 'Paseo de Santa Rosa',
            },
        ],

        express3marks: [
            {
                value: 0,
                label: 'DLSU Laguna',
            },
            {
                value: 25,
                label: 'Barangay Malamig',
            },
            {
                value: 50,
                label: 'blah',
            },
            {
                value: 75,
                label: 'blah blah',
            },
            {
                value: 100,
                label: 'Walter',
            },
        ],

        express4marks: [
            {
                value: 0,
                label: 'Mama mia',
            },
            {
                value: 25,
                label: 'Here I go again',
            },
            {
                value: 50,
                label: 'My my',
            },
            {
                value: 75,
                label: 'how can I resist you',
            },
            {
                value: 100,
                label: 'Mama mia',
            },
        ],

        express5marks: [
            {
                value: 0,
                label: 'Does it show again?',
            },
            {
                value: 25,
                label: 'My my',
            },
            {
                value: 50,
                label: 'just how much',
            },
            {
                value: 75,
                label: "I've missed you?",
            },
            {
                value: 100,
                label: 'DLSU Laguna',
            },
        ],
    };

    valuetext = (value) => {
        return `${value} meters`;
    };

    currentExpressLine = (currentDisplay) => {
        if (currentDisplay === 0) {
            return this.state.express1marks;
        } else if (currentDisplay === 1) {
            return this.state.express2marks;
        } else if (currentDisplay === 2) {
            return this.state.express3marks;
        } else if (currentDisplay === 3) {
            return this.state.express4marks;
        } else if (currentDisplay === 4) {
            return this.state.express5marks;
        }
    };


    render() {

        return (
            <div className={'sliderRoot'}>
                <Slider
                    orientation="vertical"
                    defaultValue={0}
                    getAriaValueText={this.valuetext}
                    aria-labelledby="vertical-slider"
                    step={25}
                    marks={this.currentExpressLine(this.props.currentLine)}
                    valueLabelDisplay="off"
                // track="inverted"
                />
            </div>
        );
    };
}

export default DiscreteSlider;
