import { createMuiTheme } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';
import green from '@material-ui/core/colors/green';

const botNavTheme = createMuiTheme({
    palette: {
        primary: {
            main: '#ffffff',
        },
        background: {
            default: '#087830',
            paper: '#087830'
        },
        secondary: {
            main: '#ffffff',
        }
    },
});

const topNavTheme = createMuiTheme({
    palette: {
        primary: {
            main: '#087830',
        },
        background: {
            default: '#ffffff',
            paper: '#ffffff'
        },
        secondary: {
            main: '#087830',
        }
    },
});

const stepperTheme = createMuiTheme({
    palette: {
        primary: {
            main: '#087830',
        },
        background: {
            default: '#ffffff',
            paper: '#ffffff'
        },
        secondary: {
            main: '#087830',
        }
    }
});

function BotNavTheme() {
    return botNavTheme;
}

function TopNavTheme() {
    return topNavTheme;
}

function StepperTheme() {
    return stepperTheme;
}

export { BotNavTheme, TopNavTheme, StepperTheme }
