import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { mdiBus, mdiBusClock, mdiBusMultiple, mdiVanUtility } from '@mdi/js';
import './Styles.css';
import { TopNavTheme } from './Themes.js';
import { ThemeProvider } from '@material-ui/core/styles';
//import { Icon } from '@material-ui/core';
import Icon from '@mdi/react';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';

class TripAppBar extends React.Component {
    state = {
        left: false,
        appBarTitle: 'Line 2',
        arrowsTrips: ['DLSU Laguna > DLSU Manila',
            'DLSU Laguna > Laguna Central',
            'DLSU Laguna > Pavillion Mall',
            'DLSU Laguna > Carmona',
            'DLSU Laguna > Walter',
            'DLSU Manila > DLSU Laguna',
            'Laguna Central > DLSU Laguna',
            'Pavillion Mall > DLSU Laguna',
            'Carmona > DLSU Laguna',
            'Walter > DLSU Laguna'],
        tripIndex: 0
    };

    toggleDrawer = (side, open) => event => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        this.setState({ ...this.state.left, [side]: open });
    };

    changeTitle = (displayText, newText, index) => {
        this.setState({ ...this.state.appBarTitle, [displayText]: newText });
        this.setState({ ...this.state.tripIndex, ['tripIndex']: index });
    };

    onListClick = (text, index) => event => {
        console.log(text + " " + index);
        this.changeTitle('appBarTitle', text, index);
        this.props.onChangeLine('line', index);
    }


    sideList = side => (
        <div
            className="list"
            role="presentation"
            onClick={this.toggleDrawer(side, false)}
            onKeyDown={this.toggleDrawer(side, false)}
        >
            <Typography variant="h6" color="inherit" className="listTitle">
                Outgoing Trips
            </Typography>
            <Divider />
            <List>
                {['Line 1', 'Line 2', 'Line 3', 'Line 4', 'Line 5'].map((text, index) => (
                    <ListItem button key={text}
                        onClick={this.onListClick(text, index)}>
                        <ListItemIcon>
                            <Icon path={mdiBus}
                                size={1}
                                color="#087830"
                            />
                        </ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
            <Divider />
            <Typography variant="h6" color="inherit" className="listTitle">
                Returning Trips
            </Typography>
            <Divider />
            <List>
                {['Line 1', 'Line 2', 'Line 3', 'Line 4', 'Line 5'].map((text, index) => (
                    <ListItem button key={text}
                        onClick={this.onListClick(text, index + 5)}>
                        <ListItemIcon>
                            <Icon path={mdiBus}
                                size={1}
                                color="#087830"
                            />
                        </ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
        </div>
    );

    render() {
        return (
            <ThemeProvider
                theme={TopNavTheme()}>
                <div className="root" >
                    <AppBar position="static">
                        <Toolbar variant="dense">
                            <IconButton edge="start" className="menuButton" color="inherit" aria-label="menu" onClick={this.toggleDrawer('left', true)}>
                                <MenuIcon />
                            </IconButton>
                            <div>
                                <Typography variant="h6" color="inherit">
                                    Arrows {this.state.appBarTitle}
                                </Typography>
                                <Typography variant="subtitle2" color="inherit">
                                    {this.state.arrowsTrips[this.state.tripIndex]}
                                </Typography>
                            </div>

                        </Toolbar>
                    </AppBar>

                    <div className="centeredDiv">
                        <SwipeableDrawer
                            open={this.state.left}
                            onClose={this.toggleDrawer('left', false)}
                            onOpen={this.toggleDrawer('left', true)}
                        >
                            {this.sideList('left')}
                        </SwipeableDrawer>
                    </div>
                </div>
            </ThemeProvider>

        );
    };
}


export default TripAppBar;
