import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import FolderIcon from '@material-ui/icons/Folder';
import RestoreIcon from '@material-ui/icons/Restore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import { mdiMapMarker, mdiBusClock, mdiMapMarkerQuestion, mdiBell } from '@mdi/js';
import { BotNavTheme } from './Themes.js';
import { ThemeProvider } from '@material-ui/core/styles';
import './Styles.css';
//import { Icon } from '@material-ui/core';
import Icon from '@mdi/react';
import HitchCheckpoint from '../HitchCheckpoint.js';

class BotNavBar extends React.Component {
    state = {
        value: 'trips',
    };

    handleChange = (event, newValue) => {
        this.setState({ value: newValue });
    };

    render() {
        return (
            <ThemeProvider
                theme={BotNavTheme()}>
                <BottomNavigation
                    value={this.state.value}
                    onChange={this.handleChange}
                    className="bottomNavRoot"
                >
                    <BottomNavigationAction label="Trips" value="trips" icon={
                        <Icon path={mdiMapMarker}
                            size={1}
                            color="white" />
                    } />
                    <BottomNavigationAction label="Schedules" value="scheds" icon={
                        <Icon path={mdiBusClock}
                            size={1}
                            color="white" />
                    } />
                    <BottomNavigationAction label="Announcements" value="news" icon={
                        <Icon path={mdiBell}
                            size={1}
                            color="white" />
                    } />
                    <BottomNavigationAction label="FAQs" value="faqs" icon={
                        <Icon path={mdiMapMarkerQuestion}
                            size={1}
                            color="white" />
                    } />
                </BottomNavigation>
            </ThemeProvider>

        );
    };
};


export default BotNavBar;
