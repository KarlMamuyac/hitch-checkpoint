import Firebase from 'firebase';
const firebaseConfig = {
    apiKey: "AIzaSyD1VPyoA-LTJeFa_CtNFhMkMPcLh170E40",
    authDomain: "native-tracker-eef9f.firebaseapp.com",
    databaseURL: "https://native-tracker-eef9f.firebaseio.com",
    projectId: "native-tracker-eef9f",
    storageBucket: "native-tracker-eef9f.appspot.com",
    messagingSenderId: "605798300762",
    appId: "1:605798300762:web:cef77000d54b4485b32c63",
    measurementId: "G-W9NCYFRGQK"
};
const app = Firebase.initializeApp(firebaseConfig);
export const db = app.database();
