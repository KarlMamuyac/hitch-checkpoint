import React, { Component } from 'react';
import TripAppBar from './components/TripAppBar.js';
import { Container } from '@material-ui/core';
import { Line1Stepper, Line2Stepper, Line2StepperReturn, Line3Stepper, Line3StepperReturn } from './components/Stepper.js';
import { db } from './config.js';

class HitchCheckpoint extends React.Component {
    constructor(props) {
        super(props)

        this.changeLine = this.changeLine.bind(this)
    }

    state = {
        line: 1,
        returning: false,
        lng: null,
        lat: null,
        displayedLine: null,
    }

    changeLine = (currentLine, newLine) => {
        this.setState({ ...this.state.line, [currentLine]: newLine });
        console.log("oi " + this.state.line);
    };

    getLocation() {


    };

    componentDidMount() {
        this.getLocation();
        console.log("ui " + this.state.lat);
    }


    render() {
        if (this.state.line === 1) {
            return (
                <div className='centeredDiv' >
                    <div>
                        <TripAppBar onChangeLine={this.changeLine} />
                    </div>

                    <Container className='container'>
                        <Line2Stepper currentLine={this.state.line} currentActive={0} lng={this.state.lng} lat={this.state.lat} />
                    </Container>

                </div>
            );
        } else if (this.state.line === 2) {
            return (
                <div className='centeredDiv' >
                    <div>
                        <TripAppBar onChangeLine={this.changeLine} />
                    </div>

                    <Container className='container'>
                        <Line3Stepper currentLine={this.state.line} currentActive={0} lng={this.state.lng} lat={this.state.lat} />
                    </Container>

                </div>
            );
        } else if (this.state.line === 6) {
            return (
                <div className='centeredDiv' >
                    <div>
                        <TripAppBar onChangeLine={this.changeLine} />
                    </div>

                    <Container className='container'>
                        <Line2StepperReturn currentLine={this.state.line} currentActive={0} lng={this.state.lng} lat={this.state.lat} />
                    </Container>

                </div>
            );
        } else if (this.state.line === 7) {
            return (
                <div className='centeredDiv' >
                    <div>
                        <TripAppBar onChangeLine={this.changeLine} />
                    </div>

                    <Container className='container'>
                        <Line3StepperReturn currentLine={this.state.line} currentActive={0} lng={this.state.lng} lat={this.state.lat} />
                    </Container>

                </div>
            );
        } else {
            return (
                <div className='centeredDiv' >
                    <div>
                        <TripAppBar onChangeLine={this.changeLine} />
                    </div>

                    <Container className='container'>
                        <Line2Stepper currentLine={this.state.line} currentActive={0} lng={this.state.lng} lat={this.state.lat} />
                    </Container>

                </div>
            );
        }
    };

}


export default HitchCheckpoint;
