import React from 'react';
import HitchCheckpoint from './HitchCheckpoint.js';
import BotNavBar from './components/BotNavBar.js';

function App() {

  return (
    <div className='centeredDiv'>
      <HitchCheckpoint />
      <div className="bottomNavRoot">
        <BotNavBar />
      </div>
    </div>
  );

}


export default App;
